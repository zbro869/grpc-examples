module grpc-examples

go 1.14

require (
	github.com/gogo/protobuf v1.3.1
	google.golang.org/grpc v1.30.0
)
