package main

import (
	"context"
	"log"
	"net"

	helloPb "grpc-examples/unary/pb"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type HiServer struct{}

func (s *HiServer) SayHi(ctx context.Context, req *helloPb.SayHiRequest) (*helloPb.SayHiResponse, error) {
	return &helloPb.SayHiResponse{Msg: "Hi: " + req.Msg}, nil
}

func main() {
	lis, err := net.Listen("tcp", ":6600")
	if err != nil {
		log.Printf("listen error: %v\n", err)
		return
	}

	s := grpc.NewServer()
	helloPb.RegisterHelloServiceServer(s, &HiServer{})
	reflection.Register(s)

	log.Println("Server Start...., port: ", 6600)
	if err = s.Serve(lis); err != nil {
		log.Printf("serve error: %v\n", err)
		return
	}
}
