package main

import (
	"context"
	"log"

	helloPb "grpc-examples/unary/pb"

	"google.golang.org/grpc"
)

func main() {
	address := "localhost:6600"
	ctx := context.Background()

	conn, err := grpc.DialContext(ctx, address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := helloPb.NewHelloServiceClient(conn)
	resp, err := client.SayHi(ctx, &helloPb.SayHiRequest{Msg: "Hello, My Friend~"})
	if err != nil {
		log.Printf("server error: %v\n", err)
	}
	log.Println("server response of SayHi: ", resp)
}
